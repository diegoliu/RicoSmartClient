package com.rico.smart;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class PasswordActivity extends Activity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);

        Button btn= (Button) this.findViewById(R.id.btn_save_pop);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String password=((EditText)findViewById(R.id.et_password)).getText().toString();
                String repeatPassword=((EditText)findViewById(R.id.et_repeat_password)).getText().toString();
                if(!password.trim().equals(repeatPassword)){
                    Toast.makeText(getApplicationContext(), "输入的两次密码不一致", Toast.LENGTH_LONG).show();
                }
                SharedPreferences.Editor edit =  getSharedPreferences(getPackageName(), Context.MODE_PRIVATE).edit();
                //通过editor对象写入数据
                edit.putString("password",password.trim());
                //提交数据存入到xml文件中
                edit.commit();

                Intent intent=new Intent(PasswordActivity.this,MainActivity.class);
                startActivity(intent);
            }
        });

    }
}
