package com.rico.smart;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.rico.smart.view.ClockView;

public class ClockActivity extends Activity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_clock);
        this.findViewById(R.id.clock_layout).setBackgroundColor(Color.BLACK);
        ClockView clockView=(ClockView)this.findViewById(R.id.clock);
        clockView.setBackgroundColor(Color.BLACK);

        DisplayMetrics dm = getResources().getDisplayMetrics();
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        width=width>height?height:width;
        LinearLayout.LayoutParams params= (LinearLayout.LayoutParams) clockView.getLayoutParams();
        //获取当前控件的布局对象
        params.height=width;//设置当前控件布局的高度
        params.width=width;
        clockView.setLayoutParams(params);//将设置好的布局参数应用到控件中

    }
}
