package com.rico.smart.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;


public class SlideLayout extends ViewGroup {
	private int oldX=0;
	final Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg){
            super.handleMessage(msg);
            if(msg.what == 1){
            	View child = SlideLayout.this.getChildAt(0);
            	if(child.getLeft()>0){
            		child.layout(child.getLeft()-1,child.getTop(), child.getMeasuredWidth(), child.getMeasuredHeight());
            	}
            }
        }
    };
    
	public SlideLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);		
	}

	public SlideLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public SlideLayout(Context context) {
		super(context);
	}
	 @Override  
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {  
	    super.onMeasure(widthMeasureSpec, heightMeasureSpec);  
	    if (getChildCount() > 0) {  
	        View childView = getChildAt(0);  
	        measureChild(childView, widthMeasureSpec, heightMeasureSpec);  
	    }  
	}  
	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		if (getChildCount() > 0) {  
            View childView = getChildAt(0);  
            childView.layout(0, 0, childView.getMeasuredWidth(), childView.getMeasuredHeight());  
        }  
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if(event.getAction()==MotionEvent.ACTION_DOWN){
			oldX=(int)event.getX();		
			return true;
		}
		final View child = this.getChildAt(0);
		if(child!=null){
			if(event.getAction()==MotionEvent.ACTION_MOVE){			
			
				 int moveX=(int)(event.getX()-oldX);
				 if(moveX>5||moveX<-5){
					 int newLeft=child.getLeft()+moveX;
					 if(newLeft>0){
						 child.layout(newLeft,child.getTop(), child.getMeasuredWidth(), child.getMeasuredHeight());
					 }
					 oldX=(int)event.getX();
				 }				 
			 }
			if(event.getAction()==MotionEvent.ACTION_UP||event.getAction()==MotionEvent.ACTION_CANCEL){
				if(child.getLeft()<this.getWidth()/2){
					moveToLeft();	
				 }else{
					 if(child.getLeft()>this.getWidth()/2){
						 ((Activity)SlideLayout.this.getContext()).finish();
						 return true;
					 }
				 }
			}
		}		
		return super.onTouchEvent(event);
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
	}

	private void moveToLeft(){
		final View child = this.getChildAt(0);
		Thread thread=new Thread(new Runnable(){
			@Override
			public void run() {
				while(child.getLeft()>0){
					Message message = new Message();
	                message.what = 1;
	                handler.sendMessage(message);
					try {
						Thread.sleep(1);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				SlideLayout.this.postInvalidate();
			}});
		thread.start();
		System.out.println(thread.getName()+" : moveToLeft : "+child.getWidth());
	}
}
