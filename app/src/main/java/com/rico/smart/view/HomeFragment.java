package com.rico.smart.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
;
import com.rico.smart.GalleryActivity;
import com.rico.smart.R;

public class HomeFragment extends Fragment {
	private View view;
    private static final int REQ_CODE_PERMISSION = 0x1111;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
      if(view==null){
    	   view=inflater.inflate(R.layout.home, container, false);
          Button btnButton=(Button)view.findViewById(R.id.btn_gallery);
          btnButton.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {
                  Intent starter = new Intent(getContext(), GalleryActivity.class);
                  startActivity(starter);
              }
          });
       }
       return view;
    }



    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();       
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ViewGroup p = (ViewGroup) view.getParent();  
        if(p!=null){        	
        	p.removeView(view);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


}
