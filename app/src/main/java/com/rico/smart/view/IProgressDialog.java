package com.rico.smart.view;

import android.app.ProgressDialog;
import android.content.Context;

public class IProgressDialog {
    public static ProgressDialog showProgressDialog(Context context,String title, String message) {
        ProgressDialog progressDialog=ProgressDialog.show(context, title,message, true, false);
        return progressDialog;
    }

}
