package com.rico.smart.view;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.alibaba.fastjson.JSON;
import com.jwenfeng.library.pulltorefresh.BaseRefreshListener;
import com.jwenfeng.library.pulltorefresh.PullToRefreshLayout;
import com.rico.smart.GlideImageLoader;
import com.rico.smart.R;
import com.rico.smart.adapter.DeviceAdapter;
import com.rico.smart.device.DeviceInfo;
import com.rico.smart.device.DeviceManager;
import com.rico.smart.device.Device;
import com.rico.smart.utils.Constant;

import java.util.List;

import cn.finalteam.galleryfinal.CoreConfig;
import cn.finalteam.galleryfinal.FunctionConfig;
import cn.finalteam.galleryfinal.GalleryFinal;
import cn.finalteam.galleryfinal.ImageLoader;
import cn.finalteam.galleryfinal.ThemeConfig;
import cn.finalteam.galleryfinal.model.PhotoInfo;

public class DeviceFragment extends Fragment {
    private static String TAG="DeviceFragment";
    private View view;

    private PullToRefreshLayout pullToRefreshLayout;

    private ListView listView;

    private SharedPreferences sharedPreferences;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if(view==null){
            view=inflater.inflate(R.layout.device, container, false);
            sharedPreferences = this.getActivity().getSharedPreferences(this.getActivity().getPackageName(), Context.MODE_PRIVATE);
            String deviceListJson = sharedPreferences.getString(Constant.DEVICE_LIST, "[]");
            DeviceManager.getInstance().addDeviceList(JSON.parseArray(deviceListJson, DeviceInfo.class));
            pullToRefreshLayout = (PullToRefreshLayout)view.findViewById(R.id.pull_to_refresh);
            listView=(ListView) view.findViewById(R.id.list_view);


            final DeviceAdapter deviceAdapter=new DeviceAdapter(this.getContext(), DeviceManager.getInstance().getDeviceList());
            listView.setAdapter(deviceAdapter);
            pullToRefreshLayout.setRefreshListener(new BaseRefreshListener() {
                @Override
                public void refresh() {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            // 结束刷新
                            pullToRefreshLayout.finishRefresh();
                        }
                    }, 2000);
                }

                @Override
                public void loadMore() {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            // 结束加载更多
                            pullToRefreshLayout.finishLoadMore();
                        }
                    }, 2000);
                }
            });

            DeviceManager.getInstance().addDeviceManagerListener(new DeviceManager.DeviceManagerListener() {
                @Override
                public void onDeviceChange() {
                    Log.i(TAG,"onDeviceChange");
                    Message msg=new Message();
                    msg.obj=deviceAdapter;
                    msg.what=1;
                    myHandler.sendMessage(msg);
                }
            });

            initPictureSelector();
        }
        return view;
    }



    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ViewGroup p = (ViewGroup) view.getParent();
        if(p!=null){
            p.removeView(view);
        }
    }

    private void initPictureSelector(){
        //设置主题
        ThemeConfig theme = new ThemeConfig.Builder()
                .build();
        FunctionConfig functionConfig = new FunctionConfig.Builder()
                .setEnableCamera(true)
                .setEnableEdit(true)
                .setEnableCrop(true)
                .setEnableRotate(true)
                .setCropSquare(true)
                .setEnablePreview(true)
                .build();

        ImageLoader imageloader = new GlideImageLoader();
        CoreConfig coreConfig = new CoreConfig.Builder(this.getActivity(), imageloader, theme)
                .setFunctionConfig(functionConfig)
                .build();
        GalleryFinal.init(coreConfig);
    }

    private void openPictureSelector(){

        GalleryFinal.openGalleryMuti(1,8, new GalleryFinal.OnHanlderResultCallback(){

            @Override
            public void onHanlderSuccess(int reqeustCode, List<PhotoInfo> resultList) {

            }

            @Override
            public void onHanlderFailure(int requestCode, String errorMsg) {

            }
        });
    }

    private Handler myHandler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
           switch (msg.what){
               case 1:
                   DeviceAdapter deviceAdapter=(DeviceAdapter)msg.obj;
                   deviceAdapter.notifyDataSetChanged();
                   break;
                   default:
                       break;
           }
        }
    };
}
