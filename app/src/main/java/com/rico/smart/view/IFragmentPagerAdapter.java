package com.rico.smart.view;

import java.util.List;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class IFragmentPagerAdapter extends FragmentPagerAdapter{
	private List<Fragment> fragments;
	public IFragmentPagerAdapter(FragmentManager fm,List<Fragment> fragments) {
		super(fm);
		this.fragments = fragments;
		
	}	
	@Override
	public int getCount() {
		return fragments.size();
	}	
	@Override
	public Fragment getItem(int arg0) {
		return fragments.get(arg0);
	}
	
}
