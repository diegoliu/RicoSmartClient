package com.rico.smart.view;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class IViewPager extends ViewPager {
	private boolean isCanScroll = false;

	public IViewPager(Context context) {
		super(context);
	}

	public IViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public void setScanScroll(boolean isCanScroll) {
		this.isCanScroll = isCanScroll;
	}

	@Override
	public boolean onTouchEvent(MotionEvent ev) {		
		if (isCanScroll)
			return false;
		else
			return super.onTouchEvent(ev);
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		if (!isCanScroll)
			return false;
		else
			return super.onInterceptTouchEvent(ev);
	}
}
