package com.rico.smart.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.rico.smart.MainActivity;
import com.rico.smart.R;
import com.rico.smart.device.Device;
import com.rico.smart.device.DeviceManager;
import com.rico.smart.message.MessageType;
import com.rico.smart.message.MsgMessage;
import com.rico.smart.message.UserLoginMessage;
import com.rico.smart.utils.Constant;
import com.rico.smart.utils.HandlerUtils;
import com.rico.smart.utils.ProgressDialogUtils;

import java.io.IOException;

public class LoginDeviceDialog {
    private static String TAG="LoginAlertDialog";

    private AlertDialog loginDialog;

    private Activity activity;

    private Button loginBtn;

    private  View view;

    private Device device;

    private ProgressDialog progressDialog;



    public LoginDeviceDialog(final Activity activity,final Device device){
        this.activity=activity;
        this.device=device;
        LayoutInflater inflater = (LayoutInflater) activity.getApplicationContext().getSystemService(activity.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.dialog_login, null);
        loginBtn=(Button)view.findViewById(R.id.btn_login);
        AlertDialog.Builder ad =new AlertDialog.Builder(activity);
        ad.setView(view);
        ad.setTitle("输入密码登陆");
        loginDialog=ad.create();

        final EditText password = (EditText)view.findViewById(R.id.txt_password);
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               device.getDeviceInfo().setPassword(password.getText().toString());
                progressDialog= ProgressDialogUtils.loading(activity,"","正在登陆...");
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            MsgMessage msgMessage=device.login();
                            if(msgMessage  ==null){
                                HandlerUtils.sendHandlerMessage(mHandler,0,"连接出错，请确认设备网络是否正常。");
                            }else if(msgMessage.getMessageType()== MessageType.SUCCESS){
                                device.setSessionId(msgMessage.getDeviceSessionId());
                                DeviceManager.getInstance().put(device);
                                HandlerUtils.sendHandlerMessage(mHandler,1,null);
                            }else{
                                HandlerUtils.sendHandlerMessage(mHandler,2,msgMessage.getMsg());
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            }
        });

    }

    public void show(){
        loginDialog.show();
    }
    public void dimiss(){
        loginDialog.dismiss();
    }

    public final Handler mHandler = new Handler(){
        public void handleMessage(Message msg) {
            progressDialog.hide();
            switch (msg.what) {
                case 0:
                    Toast.makeText(activity, "登陆失败，请确认网络是否正常。", Toast.LENGTH_LONG).show();
                    break;
                case 1:
                    Toast.makeText(activity, "登陆成功:", Toast.LENGTH_LONG).show();
                    SharedPreferences.Editor edit =  activity.getSharedPreferences(activity.getPackageName(), Context.MODE_PRIVATE).edit();
                    //通过editor对象写入数据
                    edit.putString(Constant.DEVICE_LIST, JSON.toJSONString(DeviceManager.getInstance().getDeviceInfoList()));
                    //提交数据存入到xml文件中
                    edit.commit();
                    loginDialog.dismiss();
                    break;
                case 2:
                    Toast.makeText(activity, "登陆失败:"+(String) msg.obj, Toast.LENGTH_LONG).show();
                    break;
                default:
                    break;
            }
        }
    };
}
