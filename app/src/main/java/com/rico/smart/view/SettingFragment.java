package com.rico.smart.view;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;


import com.alibaba.fastjson.JSON;

import com.rico.smart.R;
import com.rico.smart.activity.LoginActivity;
import com.rico.smart.activity.RegisterActivity;
import com.rico.smart.device.DeviceInfo;
import com.uuzuche.lib_zxing.activity.CodeUtils;

public class SettingFragment extends Fragment {

	private View view;

	private ImageView iv_qr_code;

	private LinearLayout llUser;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
      if(view==null){
    	   view=inflater.inflate(R.layout.setting, container, false);

          llUser=(LinearLayout)view.findViewById(R.id.ll_user);
          llUser.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {
                  Intent intent = new Intent(getActivity(), LoginActivity.class);
                  startActivity(intent);
              }
          });
       }
       return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ViewGroup p = (ViewGroup) view.getParent();  
        if(p!=null){        	
        	p.removeView(view);
        }
    }
}
