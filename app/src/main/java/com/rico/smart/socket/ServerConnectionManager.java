package com.rico.smart.socket;

import android.content.Context;
import android.content.Intent;

import com.rico.smart.activity.LoginActivity;
import com.rico.smart.device.DeviceInfo;
import com.rico.smart.message.IMessage;
import com.rico.smart.message.UserLoginMessage;
import com.rico.smart.message.MessageType;
import com.rico.smart.message.MsgMessage;
import com.rico.smart.message.UserRegisterMessage;
import com.rico.smart.user.UserInfo;
import com.rico.smart.user.UserManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ServerConnectionManager implements MessageHandler{
    private static ServerConnectionManager instance;

    private SocketClient serverSocketClient=null;
    private String serverHost="192.168.1.188";

    private int serverPort=5002;

    private String sessionId;

    private List<MessageHandler> messageHandleres=new ArrayList<>();

    private Context context;

    private ServerConnectionManager(){

    }

    public static ServerConnectionManager getInstance(){
        if(instance==null){
            instance=new ServerConnectionManager();
        }
        return instance;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public MsgMessage register(UserInfo userInfo) throws IOException {
        IMessage message=new UserRegisterMessage(userInfo);
        return (MsgMessage)this.sendMessageAndWaitResponse(message);
    }

    public MsgMessage login(String loginName,String password) throws IOException {
        UserLoginMessage message=new UserLoginMessage();
        message.setLoginName(loginName);
        message.setLoginPassword(password);
        return (MsgMessage)this.sendMessageAndWaitResponse(message);
    }


    private void ensure() throws IOException {
        if(serverSocketClient==null) {
            serverSocketClient = new SocketClient(serverHost, serverPort);
        }
        if(!serverSocketClient.isConnected()) {
            serverSocketClient.connect();
            serverSocketClient.setMessageHandler(this);
        }
    }

    public void sendMessage(final IMessage message) throws IOException {
        message.setSessionId(sessionId);
        if(UserManager.getInstance().getUserInfo()!=null) {
            message.setSender(UserManager.getInstance().getUserInfo().getLogin_name());
        }
        ensure();
        serverSocketClient.sendMessage(message);

    }

    public IMessage sendMessageAndWaitResponse(IMessage message,Integer waitTimeOut) throws IOException {
        message.setSessionId(sessionId);
        if(UserManager.getInstance().getUserInfo()!=null) {
            message.setSender(UserManager.getInstance().getUserInfo().getLogin_name());
        }
        ensure();
        return serverSocketClient.sendMessageAndWaitResponse(message,waitTimeOut);
    }

    public IMessage sendMessageAndWaitResponse(IMessage message) throws IOException {
       return sendMessageAndWaitResponse(message,15000);
    }


    @Override
    public void onMessageReceived(SocketConnection socketThread, IMessage message) {
        for(MessageHandler item:messageHandleres){
            item.onMessageReceived(socketThread,message);
        }
        if(message.getMessageType()==MessageType.NOT_LOGIN){
            if(message.getSender()==null||message.getSender().equals("SERVER")){
                Intent intent = new Intent(this.context, LoginActivity.class);
                context.startActivity(intent);
            }else{

            }
        }

        if(message.getSessionId()!=null) {
            this.sessionId = message.getSessionId();
        }
    }

    @Override
    public void onReceivedMessageError(SocketConnection socketThread, Exception e) {
        synchronized (this) {
            this.notifyAll();
        }
    }

    public void addMessageHandler(MessageHandler messageHandler){
        messageHandleres.add(messageHandler);
    }
}
