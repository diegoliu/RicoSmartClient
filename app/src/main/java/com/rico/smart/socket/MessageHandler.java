package com.rico.smart.socket;

import com.rico.smart.message.IMessage;


public interface MessageHandler {
    void onMessageReceived(SocketConnection socketThread, IMessage message);

    void onReceivedMessageError(SocketConnection socketThread,Exception e);
}
