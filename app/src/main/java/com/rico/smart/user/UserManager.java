package com.rico.smart.user;

public class UserManager {
    private static UserManager instance;

    public UserInfo userInfo;

    public static UserManager getInstance(){
        if(instance==null){
            instance=new UserManager();
        }
        return instance;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public String getSessionId() {
        return userInfo.getSessionId();
    }

    public void setSessionId(String sessionId) {
        userInfo.setSessionId(sessionId);
    }
}
