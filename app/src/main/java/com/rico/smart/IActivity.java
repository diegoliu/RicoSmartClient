package com.rico.smart;

import android.app.Activity;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;

import com.rico.smart.view.SlideLayout;

public class IActivity extends Activity {

	public static final int ANIMATION_TIME = 3000;
	private GestureDetector detector;
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		ViewGroup localViewGroup = (ViewGroup)getWindow().getDecorView();
		View localView;
		SlideLayout localSlideLayout = new SlideLayout(this);
		LayoutParams layoutParams=new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		localSlideLayout.setLayoutParams(layoutParams);
		
		localView = localViewGroup.getChildAt(0);
		localViewGroup.removeView(localView); 
		localSlideLayout.addView(localView);	
	    localViewGroup.addView(localSlideLayout);


		detector = new GestureDetector(this, new  GestureDetector.OnGestureListener(){

			@Override
			public boolean onDown(MotionEvent e) {
				return false;
			}

			@Override
			public void onShowPress(MotionEvent e) {

			}

			@Override
			public boolean onSingleTapUp(MotionEvent e) {
				return false;
			}

			@Override
			public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
				return false;
			}

			@Override
			public void onLongPress(MotionEvent e) {

			}

			@Override
			public boolean onFling(MotionEvent e1, MotionEvent e2,
								   float velocityX, float velocityY) {
				if(e2.getX() - e1.getX() > 100 && Math.abs(velocityX) > 100){
					finish();
				}
				return false;
			}});

	    super.onPostCreate(savedInstanceState);
	   
	}
	
}
