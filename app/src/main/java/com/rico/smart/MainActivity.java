package com.rico.smart;


import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.rico.smart.device.Device;
import com.rico.smart.device.DeviceManager;
import com.rico.smart.device.DeviceInfo;
import com.rico.smart.socket.ServerConnectionManager;
import com.rico.smart.utils.Constant;
import com.rico.smart.utils.FileUtils;
import com.rico.smart.utils.HandlerUtils;
import com.rico.smart.view.DeviceFragment;
import com.rico.smart.view.LoginDeviceDialog;
import com.rico.smart.view.ScanFragment;
import com.rico.smart.view.SettingFragment;
import com.uuzuche.lib_zxing.activity.CaptureActivity;
import com.uuzuche.lib_zxing.activity.CodeUtils;
import com.uuzuche.lib_zxing.activity.ZXingLibrary;

import java.util.ArrayList;
import java.util.List;



public class MainActivity extends FragmentActivity  implements ViewPager.OnPageChangeListener {

    protected static final String TAG="MainActivity";

    private RadioGroup rgs;
    public List<Fragment> fragments = new ArrayList<Fragment>();
    private ViewPager viewPager;

    private SharedPreferences sharedPreferences;

    private LoginDeviceDialog loginDialog;

    private static final int REQ_CODE_PERMISSION = 0x1111;


    private static final int SHOW_LOGIN_UI=1;

    private static final int SHOW_LOGIN_SUCCESS=2;

    private static final int SHOW_LOGIN_FAIL=3;

    private static final int SHOW_NOTIFY=4;

    private static final int SHOW_LOADING=5;

    private static final int DISSMIS_LOADING=6;

    private ProgressDialog progressDialog;

    public static MainActivity instance=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        ServerConnectionManager.getInstance().setContext(this);
        instance=this;
        // 去掉界面任务条
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        initView();

        sharedPreferences = getSharedPreferences(this.getPackageName(), Context.MODE_PRIVATE);

        ZXingLibrary.initDisplayOpinion(this);

        FileUtils.checkPermission(MainActivity.this);


    }

    private void initView(){
        rgs = (RadioGroup) findViewById(R.id.tabs_rg);
        //fragments.add(new HomeFragment());
        fragments.add(new DeviceFragment());
        fragments.add(new ScanFragment());
        fragments.add(new SettingFragment());
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        viewPager.setOnPageChangeListener(this);
        viewPager.setAdapter(new IFragmentPagerAdapter(getSupportFragmentManager(), fragments));
        viewPager.setCurrentItem(0);//设置当前显示标签页为第一页
        RadioButton radioButton=(RadioButton)rgs.getChildAt(0);
        radioButton.setTextColor(getResources().getColor(R.color.radio_color_checked));
        rgs.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                int childCount=group.getChildCount();
                for(int i=0;i<childCount;i++){
                    RadioButton radioButton=(RadioButton)group.getChildAt(i);
                    if(radioButton.getId()!=checkedId){
                        radioButton.setTextColor(getResources().getColor(R.color.radio_color));
                    }else{
                        radioButton.setTextColor(getResources().getColor(R.color.radio_color_checked));
                    }
                }
                int fragmentIndex=0;
                switch(checkedId){
                   /* case R.id.radio_home:
                        fragmentIndex=0;
                        break;*/
                    case R.id.radio_device:
                        fragmentIndex=0;
                        break;
                    case R.id.radio_scan:
                        fragmentIndex=1;
                        scanQRCode();
                        break;
                    case R.id.radio_setting:
                        fragmentIndex=2;
                        break;
                }
                viewPager.setCurrentItem(fragmentIndex,false);
            }});
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }



    @Override
    public void onPageScrollStateChanged(int arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onPageSelected(int arg0) {
        for(int i=0;i<rgs.getChildCount();i++){
            RadioButton radioButton=(RadioButton)rgs.getChildAt(i);
            if(i!=arg0){
                radioButton.setChecked(false);
                radioButton.setTextColor(getResources().getColor(R.color.radio_color));
            }else{
                radioButton.setChecked(true);
                radioButton.setTextColor(getResources().getColor(R.color.radio_color_checked));
            }
        }
    }



    public void scanQRCode(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            // Do not have the permission of camera, request it.
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, REQ_CODE_PERMISSION);
        } else {
            // Have gotten the permission
            Intent intent = new Intent(this, CaptureActivity.class);
            startActivityForResult(intent,1);
        }
    }

    // 为了获取结果
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (null != data) {
            Bundle bundle = data.getExtras();
            if (bundle == null) {
                return;
            }
            if (bundle.getInt(CodeUtils.RESULT_TYPE) == CodeUtils.RESULT_SUCCESS) {
                String result = bundle.getString(CodeUtils.RESULT_STRING);
                Toast.makeText(this, "解析结果:" + result, Toast.LENGTH_LONG).show();
                DeviceInfo deviceInfo= JSON.parseObject(result,DeviceInfo.class);
                Device device=DeviceManager.getInstance().get(deviceInfo.getDeviceId());
                if(device==null){
                    device=new Device();
                    DeviceManager.getInstance().put(device);
                }
                device.setDeviceInfo(deviceInfo);
                loginDialog=new LoginDeviceDialog(MainActivity.this,device);
                loginDialog.show();
            } else if (bundle.getInt(CodeUtils.RESULT_TYPE) == CodeUtils.RESULT_FAILED) {
                Toast.makeText(MainActivity.this, "解析二维码失败", Toast.LENGTH_LONG).show();
            }
        }
    }

}
