package com.rico.smart.utils;

import android.app.Activity;
import android.app.ProgressDialog;


public class ProgressDialogUtils {

    public static ProgressDialog loading(Activity activity,String title, String content){
       return  ProgressDialog.show(activity, title,content, true, false);
    }
}
