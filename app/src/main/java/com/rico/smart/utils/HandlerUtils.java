package com.rico.smart.utils;

import android.os.Handler;
import android.os.Message;

public class HandlerUtils {
    public static void sendHandlerMessage(Handler handler, int what, Object obj){
        Message msg = new Message();
        msg.what=what;
        msg.obj=obj;
        handler.sendMessage(msg);
    }
}
