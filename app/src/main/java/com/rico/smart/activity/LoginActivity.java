package com.rico.smart.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.rico.smart.IActivity;
import com.rico.smart.R;
import com.rico.smart.device.DeviceManager;
import com.rico.smart.message.MessageType;
import com.rico.smart.message.MsgMessage;
import com.rico.smart.socket.ServerConnectionManager;
import com.rico.smart.user.UserInfo;
import com.rico.smart.user.UserManager;
import com.rico.smart.utils.Constant;
import com.rico.smart.utils.SharedPreferencesHelper;

import java.io.IOException;

public class LoginActivity extends IActivity {

    EditText etLoginName;
    EditText etPassword;
    Button button;

    private SharedPreferencesHelper sharedPreferencesHelper;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);

        ImageView imageView=(ImageView)this.findViewById(R.id.iv_close);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginActivity.this.finish();
            }
        });

        this.etLoginName=(EditText)this.findViewById(R.id.et_login_name);
        this.etPassword=(EditText)this.findViewById(R.id.et_password);

        this.button=(Button)this.findViewById(R.id.btn_submit);
        this.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String loginName=etLoginName.getText().toString();
                final String password=etPassword.getText().toString();
                if(loginName==null||loginName.trim().equals("")){
                    Toast.makeText(LoginActivity.this, "用户名不能为空", Toast.LENGTH_LONG).show();
                    return;
                }

                if(password==null||password.trim().equals("")){
                    Toast.makeText(LoginActivity.this, "密码不能为空", Toast.LENGTH_LONG).show();
                    return;
                }


               new Thread(new Runnable() {
                   @Override
                   public void run() {
                       try {
                          MsgMessage iMessage= ServerConnectionManager.getInstance().login(loginName,password);
                          if(iMessage==null){
                              sendHandlerMessage(2,"登陆出错，请检测网络是否正常!");
                          }
                          else if(iMessage.getMessageType()== MessageType.SUCCESS){
                              UserInfo userInfo=new UserInfo();
                              userInfo.setLogin_name(loginName);
                              userInfo.setPassword(password);
                              UserManager.getInstance().setUserInfo(userInfo);
                              UserManager.getInstance().setSessionId(iMessage.getSessionId());

                              SharedPreferences.Editor editor=SharedPreferencesHelper.editor(LoginActivity.this);
                              editor.putString(Constant.LOGIN_USER, JSON.toJSONString(userInfo));
                              //提交数据存入到xml文件中
                              editor.commit();


                              sendHandlerMessage(1,"登陆成功！");
                          }else{
                              sendHandlerMessage(2,"登陆失败!"+(iMessage==null?"":iMessage.getMsg()));
                          }
                       } catch (IOException e) {
                           e.printStackTrace();
                       }
                   }
               }).start();


            }
        });
    }
    private Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case 1:
                    Toast.makeText(LoginActivity.this, (String)msg.obj, Toast.LENGTH_LONG).show();
                    LoginActivity.this.finish();
                    break;
                case 2:
                    Toast.makeText(LoginActivity.this, (String)msg.obj, Toast.LENGTH_LONG).show();
                    break;
            }
        }
    };
    public void sendHandlerMessage(int what,Object obj){
        Message msg=new Message();
        msg.what=what;
        msg.obj=obj;
        handler.sendMessage(msg);
    }
}
