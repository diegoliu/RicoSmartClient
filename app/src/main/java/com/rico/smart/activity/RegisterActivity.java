package com.rico.smart.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.rico.smart.IActivity;
import com.rico.smart.R;
import com.rico.smart.message.MessageType;
import com.rico.smart.message.MsgMessage;
import com.rico.smart.socket.ServerConnectionManager;
import com.rico.smart.user.UserInfo;

import java.io.IOException;

public class RegisterActivity extends IActivity {

    EditText etLoginName;
    EditText etPassword;
    EditText etRepeatPassword;
    Button button;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_register);

        ImageView imageView=(ImageView)this.findViewById(R.id.iv_close);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RegisterActivity.this.finish();
            }
        });

        this.etLoginName=(EditText)this.findViewById(R.id.et_login_name);
        this.etPassword=(EditText)this.findViewById(R.id.et_password);
        this.etRepeatPassword=(EditText)this.findViewById(R.id.et_repeat_password);
        this.button=(Button)this.findViewById(R.id.btn_submit);
        this.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String loginName=etLoginName.getText().toString();
                String password=etPassword.getText().toString();
                String repeatPassword=etRepeatPassword.getText().toString();
                if(loginName==null||loginName.trim().equals("")){
                    Toast.makeText(RegisterActivity.this, "用户名不能为空", Toast.LENGTH_LONG).show();
                    return;
                }

                if(password==null||password.trim().equals("")){
                    Toast.makeText(RegisterActivity.this, "密码不能为空", Toast.LENGTH_LONG).show();
                    return;
                }
                if(!password.trim().equals(repeatPassword)){
                    Toast.makeText(RegisterActivity.this, "两次输入密码不一致", Toast.LENGTH_LONG).show();
                    return;
                }

                final UserInfo userInfo=new UserInfo();
                userInfo.setLogin_name(loginName);
                userInfo.setPassword(password);
               new Thread(new Runnable() {
                   @Override
                   public void run() {
                       try {
                          MsgMessage iMessage= ServerConnectionManager.getInstance().register(userInfo);
                          if(iMessage==null){
                              sendHandlerMessage(2,"注册出错，请确定网络是否正常!");
                          }
                          else if(iMessage.getMessageType()== MessageType.SUCCESS){
                              sendHandlerMessage(1,"注册成功！");
                          }else{
                              sendHandlerMessage(2,"注册失败!"+(iMessage==null?"":iMessage.getMsg()));
                          }
                       } catch (IOException e) {
                           e.printStackTrace();
                       }
                   }
               }).start();


            }
        });
    }
    private Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case 1:
                    Toast.makeText(RegisterActivity.this, (String)msg.obj, Toast.LENGTH_LONG).show();
                    RegisterActivity.this.finish();
                    break;
                case 2:
                    Toast.makeText(RegisterActivity.this, (String)msg.obj, Toast.LENGTH_LONG).show();
                    break;
            }
        }
    };
    public void sendHandlerMessage(int what,Object obj){
        Message msg=new Message();
        msg.what=what;
        msg.obj=obj;
        handler.sendMessage(msg);
    }
}
