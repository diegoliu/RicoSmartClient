package com.rico.smart;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.alibaba.fastjson.JSON;
import com.rico.smart.user.UserInfo;
import com.rico.smart.user.UserManager;
import com.rico.smart.utils.Constant;


public class LaucherActivity extends Activity {

    SharedPreferences sharedPreferences = null;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laucher);
        sharedPreferences = getSharedPreferences(this.getPackageName(), Context.MODE_PRIVATE);
        String password = sharedPreferences.getString("password", "");

        String userJsonString=sharedPreferences.getString(Constant.LOGIN_USER,"[]");
        UserInfo userInfo= JSON.parseObject(userJsonString,UserInfo.class);
        if(userInfo!=null){
            UserManager.getInstance().setUserInfo(userInfo);
        }
        if(password==null||password.trim().equals("")) {
            Intent intent=new Intent(LaucherActivity.this,PasswordActivity.class);
            startActivity(intent);
        }else{
            Intent intent=new Intent(LaucherActivity.this,MainActivity.class);
            startActivity(intent);
        }
    }
}
