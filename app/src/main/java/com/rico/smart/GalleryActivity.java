package com.rico.smart;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.rico.smart.utils.Constant;
import com.rico.smart.utils.FileUtils;
import com.youth.banner.Banner;
import com.youth.banner.BannerConfig;
import com.youth.banner.Transformer;
import com.youth.banner.loader.ImageLoader;

import java.io.File;
import java.util.List;

public class GalleryActivity extends IActivity {
    public static final int REQUEST_CODE = 0x1;

    private Banner banner;
    //设置图片资源:url或本地资源
    String[] images= new String[] {
            "http://img.zcool.cn/community/0166c756e1427432f875520f7cc838.jpg",
            "http://img.zcool.cn/community/018fdb56e1428632f875520f7b67cb.jpg",
            "http://img.zcool.cn/community/01c8dc56e1428e6ac72531cbaa5f2c.jpg",
            "http://img.zcool.cn/community/01fda356640b706ac725b2c8b99b08.jpg",
            "http://img.zcool.cn/community/01fd2756e142716ac72531cbf8bbbf.jpg",
            "http://img.zcool.cn/community/0114a856640b6d32f87545731c076a.jpg"};

    //设置图片标题:自动对应
    String[] titles=new String[]{"全场2折起","十大星级品牌联盟","嗨购5折不要停","12趁现在","嗨购5折不要停，12.12趁现在","实打实大顶顶顶顶"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_gallery);

        banner = (Banner) findViewById(R.id.banner);
        //设置banner样式
        //banner.setBannerStyle(BannerConfig.LEFT);
        //设置图片加载器
        banner.setImageLoader(new GlideImageLoader());

        //设置图片集合
        //banner.setImages(Arrays.asList(images));
        String file= Constant.IMAGE_PATH;
        List<File> files=FileUtils.listFiles(new File(file));
        banner.setImages(files);
        //设置banner动画效果
        banner.setBannerAnimation(Transformer.Default);

        //设置标题集合（当banner样式有显示title时）
        //banner.setBannerTitles(Arrays.asList(titles));
        //设置自动轮播，默认为true
        banner.isAutoPlay(true);
        //设置轮播时间
        banner.setDelayTime(3000);
        //设置指示器位置（当banner模式中有指示器时）
        banner.setIndicatorGravity(BannerConfig.CENTER);
        //banner设置方法全部调用完毕时最后调用
        banner.start();
        flushPictures();
    }


    public class GlideImageLoader extends ImageLoader {
        @Override
        public void displayImage(Context context, Object path, ImageView imageView) {
            System.out.println("加载中");

            Glide.with(getApplicationContext()).load(path).asBitmap().into(imageView);

            System.out.println("加载完");
        }

    }

    public void flushPictures(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true){
                    Message msg=new Message();
                    msg.what=0;
                    ihandler.sendMessage(msg);
                    try {
                        Thread.sleep(3000l);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    private Handler ihandler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case 1:
                    String file= Constant.IMAGE_PATH;
                    List<File> files=FileUtils.listFiles(new File(file));
                    banner.update(files);
                    break;
            }
        }
    };

    public static void setImageViewMathParent(Activity context,
                                              ImageView image,Bitmap bitmap) {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        float scalew = (float) displayMetrics.widthPixels / (float) bitmap.getWidth();
        image.setScaleType(ImageView.ScaleType.MATRIX);
        Matrix matrix = new Matrix();
        image.setAdjustViewBounds(true);
        if (displayMetrics.widthPixels < bitmap.getWidth()) {
            matrix.postScale(scalew, scalew);
        } else {
            matrix.postScale(1 / scalew, 1 / scalew);        }
        image.setMaxWidth(displayMetrics.widthPixels);
        float ss = displayMetrics.heightPixels > bitmap.getHeight() ? displayMetrics.heightPixels : bitmap.getHeight();
        image.setMaxWidth((int) ss);

        if (bitmap != null && bitmap.isRecycled()) {
            bitmap.recycle();
        }
    }
}
