package com.rico.smart.message;

public abstract class IMessage {

    private String sender;

    private String receiver;

    private String sessionId;

    private String deviceSessionId;

    private int clientType=0;

    private Long messageId;

    private Long respMessageId;

    public abstract byte[] toBytes();

    public abstract MessageType getMessageType();


    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public int getClientType() {
        return clientType;
    }

    public void setClientType(int clientType) {
        this.clientType = clientType;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Long getMessageId() {
        return messageId;
    }

    public void setMessageId(long messageId) {
        this.messageId = messageId;
    }

    public Long getRespMessageId() {
        return respMessageId;
    }

    public void setRespMessageId(long respMessageId) {
        this.respMessageId = respMessageId;
    }

    public String getDeviceSessionId() {
        return deviceSessionId;
    }

    public void setDeviceSessionId(String deviceSessionId) {
        this.deviceSessionId = deviceSessionId;
    }
}
