package com.rico.smart.message;

import com.alibaba.fastjson.JSON;
import com.rico.smart.user.UserInfo;


import java.io.UnsupportedEncodingException;

public class UserRegisterMessage extends IMessage {

    private UserInfo userInfo;

    public UserRegisterMessage(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    @Override
    public byte[] toBytes() {
        try {
            return JSON.toJSONString(this).getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public MessageType getMessageType() {
        return MessageType.REGISTER;
    }
}
