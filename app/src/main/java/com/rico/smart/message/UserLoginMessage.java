package com.rico.smart.message;


import com.alibaba.fastjson.JSON;

import java.io.UnsupportedEncodingException;

public class UserLoginMessage extends IMessage {

   private String loginName;

    private String loginPassword;


    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getLoginPassword() {
        return loginPassword;
    }

    public void setLoginPassword(String loginPassword) {
        this.loginPassword = loginPassword;
    }



    @Override
    public byte[] toBytes() {
        try {
            return JSON.toJSONString(this).getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public MessageType getMessageType() {
       return MessageType.LOGIN;
    }
}
