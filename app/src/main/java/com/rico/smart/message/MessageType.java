package com.rico.smart.message;

public enum MessageType {
    CONNECT,LOGIN,LOGOUT,SUCCESS,ERROR,MESSAGE,TEST,SCREEN,IMAGE,FILE,NOT_LOGIN,REGISTER;
    public static MessageType getMessageType(int value) {
        return MessageType.values()[value];
    }
}
