package com.rico.smart.message;


import com.alibaba.fastjson.JSON;

public class ImageMessage extends IMessage {


    private String fileName;
    private String extName;
    private byte imageBytes[];
    public ImageMessage(){}

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getExtName() {
        return extName;
    }

    public void setExtName(String extName) {
        this.extName = extName;
    }

    public byte[] getImageBytes() {
        return imageBytes;
    }

    public void setImageBytes(byte[] imageBytes) {
        this.imageBytes = imageBytes;
    }

    @Override
    public byte[] toBytes() {
        return JSON.toJSONBytes(this);
    }

    @Override
    public MessageType getMessageType() {
        return MessageType.IMAGE;
    }
}
