package com.rico.smart.message;

import com.alibaba.fastjson.JSON;

import java.io.UnsupportedEncodingException;

public class MsgMessage extends IMessage {

    private MessageType messageType;

    private String msg;

    public MsgMessage(){}

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public MsgMessage(MessageType messageType, String msg) {
        this.messageType = messageType;
        this.msg = msg;
    }

    @Override
    public byte[] toBytes() {
        try {
            return JSON.toJSONString(this).getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public MessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(MessageType messageType) {
        this.messageType = messageType;
    }
}
